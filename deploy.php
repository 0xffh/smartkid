<?php

require 'recipe/symfony3.php';
$parameters = \Symfony\Component\Yaml\Yaml::parse(file_get_contents('app/config/parameters.yml'));

// Set configurations
set('repository', 'git@bitbucket.org:0xffh/smartkid.git');
set('copy_dirs', ['vendor']);
set('shared_dirs', ['var/logs', 'var/sessions', 'web/uploads/gallery', 'var/jwt']);
set('writable_dirs', ['web/uploads/gallery', 'var/cache', 'var/logs']);
env('SMARTKID_DATABASE_NAME', 'smartkid-prod');

before('deploy:vendors', 'deploy:copy_dirs');

// Configure servers
server('dev', 'k-3soft.com', 2222)
    ->user($parameters['parameters']['deploy_user'])
    ->forwardAgent()
    ->stage('development')
    ->env('deploy_path', '/usr/local/www/api-smartkid-dev')
    ->env('branch', 'dev')
    ->env('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction')
    ->env('env_vars', 'SYMFONY_ENV=dev')
    ->env('env', 'dev')
;

server('prod', 'k-3soft.com', 2222)
    ->user($parameters['parameters']['deploy_user'])
    ->forwardAgent()
    ->stage('production')
    ->env('deploy_path', '/usr/local/www/api-smartkid-prod')
;

task('deploy:writable', function () {
    $dirs = join(' ', get('writable_dirs'));
    $sudo = get('writable_use_sudo') ? 'sudo' : '';
    if (!empty($dirs)) {
        try {
            cd('{{release_path}}');
            run("sudo chmod -R 777 $dirs");
        } catch (\RuntimeException $e) {
            $formatter = \Deployer\Deployer::get()->getHelper('formatter');
            $errorMessage = [
                "Unable to setup correct permissions for writable dirs.                  ",
                "You need to configure sudo's sudoers files to not prompt for password,",
                "or setup correct permissions manually.                                  ",
            ];
            write($formatter->formatBlock($errorMessage, 'error', true));
            throw $e;
        }
    }
})->desc('Make writable dirs');

task('deploy:clear_controllers', function () {
    run("rm -f {{release_path}}/web/app_*.php");
    run("rm -f {{release_path}}/web/config.php");
})->setPrivate()->onlyOn('prod');

/**
 * Cleanup old releases.
 */
task('cleanup', function () {
    $releases = env('releases_list');

    $keep = get('keep_releases');

    while ($keep > 0) {
        array_shift($releases);
        --$keep;
    }

    foreach ($releases as $release) {
        run("sudo rm -rf {{deploy_path}}/releases/$release");
    }

    run("cd {{deploy_path}} && if [ -e release ]; then rm release; fi");
    run("cd {{deploy_path}} && if [ -h release ]; then rm release; fi");

})->desc('Cleaning up old releases');

after('deploy:vendors', 'database:migrate');

/**
 * Restart php-fpm on success deploy.
 */
//task('php-fpm:restart', function () {
//    // Attention: The user must have rights for restart service
//    // Attention: the command "sudo /bin/systemctl restart php-fpm.service" used only on CentOS system
//    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
//    run('sudo /bin/systemctl restart php-fpm.service');
//})->desc('Restart PHP-FPM service');
//
//after('success', 'php-fpm:restart');