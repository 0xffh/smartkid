<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController as RestController;
use FOS\RestBundle\Controller\Annotations as REST;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class AccountController
 * @package AppBundle\Controller\Api
 */
class AccountController extends RestController
{
    /**
     * @ApiDoc(section="Account", description="Отображает информацию по текущему залогиненому юзеру")
     *
     * @REST\Route("accounts/", methods={"GET"})
     *
     * @return \FOS\RestBundle\View\View
     */
    public function getAccountAction()
    {
        $user = $this->getUser();

        $context = (new Context())->setGroups(['show_user_full']);

        return $this->view($user)->setContext($context);
    }
}