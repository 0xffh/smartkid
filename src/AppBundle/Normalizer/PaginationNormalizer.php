<?php

namespace AppBundle\Normalizer;

use Knp\Component\Pager\Pagination\AbstractPagination;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PaginationNormalizer extends ObjectNormalizer
{
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof AbstractPagination;
    }
}