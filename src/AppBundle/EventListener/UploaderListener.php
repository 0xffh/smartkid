<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Media\Image;
use Doctrine\Common\Persistence\ObjectManager;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\HttpFoundation\File\File;

class UploaderListener
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function onUpload(PostPersistEvent $event)
    {
        /** @var File $file */
        $file = $event->getFile();
        $pathName = $file->getPathName();

        $originSlug = mb_substr($pathName, mb_strpos($pathName, "/uploads"));

        $image = new Image();
        $image->setPath($originSlug);

        $this->objectManager->persist($image);
        $this->objectManager->flush();

        $response = $event->getResponse();
        $response['id'] = $image->getId();
        $response['path'] = $image->getPath();
    }
}
