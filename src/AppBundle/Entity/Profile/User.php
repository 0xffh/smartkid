<?php

namespace AppBundle\Entity\Profile;

use K3Soft\Bundle\FOSUserBridgeBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->username = uniqid('', true) . random_int(PHP_INT_MIN, PHP_INT_MAX);
    }
}
